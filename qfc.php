<?php

/**
 * Plugin Name:       QSM Filters and Charts
 * Description:       Filters and charts Addo for Quiz & Survey Master Plugin
 * Version:           1.0.0
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       qfc
 */
require_once rtrim(__DIR__, '/\\') . '/qfc-config.php';

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'qfc_options_install');

// function to create the DB / Options / Defaults
function qfc_options_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . "mlw_question_difficulty";
    $table_name1 = $wpdb->prefix . "mlw_modified_results";
    $table_name2 = $wpdb->prefix . "mlw_modified_results1";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
        dbDelta("CREATE TABLE " . $table_name . " (
		`id` mediumint(9) NOT NULL AUTO_INCREMENT,
		`quiz_id` int(11) NOT NULL,
		`question_id` int(11) NOT NULL,
		`difficulty_level` varchar(50) NOT NULL,
		UNIQUE KEY id (id)
		);");
    }
    if ($wpdb->get_var("show tables like '$table_name1'") != $table_name1) {
        require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
        dbDelta("CREATE TABLE " . $table_name1 . " (
		`id` int(11) NOT NULL AUTO_INCREMENT,
                `quiz_id` int(11) NOT NULL,
                `user_id` int(11) NOT NULL,
                `cat_name` varchar(50) NOT NULL,
                `quiz_name` text NOT NULL,
                `correct_score` double NOT NULL,
                `user_name` varchar(255) NOT NULL,
                `deleted` int(11) NOT NULL,
                PRIMARY KEY (`id`)
		);");
    }
    if($wpdb->get_var("show tables like '$table_name2'") != $table_name2) {
        require_once(ABSPATH . 'wp-admin/upgrade-functions.php');
        dbDelta("CREATE TABLE " . $table_name2 . " (
		`id` int(11) NOT NULL AUTO_INCREMENT,
                `quiz_id` int(11) NOT NULL,
                `user_id` int(11) NOT NULL,
                `cat_name` varchar(50) NOT NULL,
                `question_name` varchar(50) NOT NULL,
                `selected_option` varchar(255) NOT NULL,
                `quiz_name` text NOT NULL,
                `correct_score` double NOT NULL,
                `user_name` varchar(255) NOT NULL,
                `deleted` int(11) NOT NULL,
                PRIMARY KEY (`id`)
		);");
    }
}

//functionality for auto deativating and prevent activation if the Required plugin is not active
{
    //name and slug for required plugin
    {
        $required_plugin_slug = "quiz-master-next";
        $required_plugin_name = "Quiz And Survey Master";
    }

    //get active plugins
    $active_plugins = get_option('active_plugins');

    //check if the required plugin is active?
    {
        $got_plugin = FALSE;
        foreach ($active_plugins as $active_plugin) {
            if (strpos($active_plugin, $required_plugin_slug) !== FALSE) {
                $got_plugin = true;
                break;
            }
        }
    }

    //deactivate the plugin if required plugin is not active
    if (!$got_plugin) {
        if (!function_exists('deactivate_plugins'))
            include_once ABSPATH . 'wp-admin/includes/plugin.php';
        deactivate_plugins(plugin_basename(__FILE__));
        wp_die('<b><font color="red">QSM Filters and Charts</font></b> plugin requires <b>' . $required_plugin_name . '</b>.<hr />Please activate the required plugin first.');
    }
}
//default difficulties
{
    $default_difficulties = array(
        'easy' => 'Easy',
        'medium' => 'Medium',
        'hard' => 'Hard',
    );
    update_option(QFC_DIFFICULTIES, $default_difficulties);
}
/** Begin Plugin Execution * */
add_filter('qmn_begin_shortcode', 'qfc_add_filters_to_quiz', 10, 2);
add_filter('qmn_begin_question_query', 'qfc_apply_filters_to_quiz', 10, 2);
//add_action('mlw_qmn_ud_after_quiz_options', 'qfc_mlw_qmn_ud_after_quiz_options');
add_filter('qmn_end_results', 'qfc_mlw_qmn_ud_after_question', 10, 2);
add_action('mlw_qmn_show_question_difficulty', 'qfc_show_question_difficulty', 10);
add_action('qmn_add_question_difficiulty_level', 'qfc_insert_question_difficulty', 10, 4);
add_action('wp_ajax_qfc_get_question_difficulty', 'qfc_get_question_difficulty');
add_action('qmn_after_saving_result','qfc_after_saving_user_result',10,2);
add_filter('mlw_after_result_table', 'qfc_add_graph_on_dashboard');
add_filter('qmn_qsm_after_each_question', 'qfc_add_graph_on_each_question',10, 5);
add_action( 'wp_enqueue_scripts', 'get_question_answer_to_make_graph' );
add_action('wp_ajax_qfc_get_question_answer', 'qfc_get_question_answer');