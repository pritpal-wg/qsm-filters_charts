<?php

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

//@ini_set('auto_detect_line_endings', TRUE);
//defines
{
    //path and url
    define('QFC_DIR_PATH', untrailingslashit(__DIR__) . '/');
    define('QFC_DIR_URL', untrailingslashit(plugin_dir_url(__FILE__)) . '/');
    require_once QFC_DIR_PATH . 'includes/qfc-functions.php';

    //domain & sttings group
    define('QFC_DOMAIN', 'qfc');
    define('QFC_SETTINGS_GROUP', 'qfc_settings_group');

    //option for saving the default difficulties
    define('QFC_DIFFICULTIES', 'qfc_default_difficulties');

    //table_prefix
//    define('QFC_PREFIX', $wpdb->prefix . 'qfc_');
    //tables for plugin
    {
//        define('WL_TABLE_CAMPAIGNS', WL_PREFIX . 'campaigns');
//        define('WL_TABLE_CAMPAIGN_TEMPLATES', WL_PREFIX . 'campaign_templates');
//        define('WL_TABLE_CAMPAIGN_SCHEDULE', WL_PREFIX . 'campaign_schedule');
    }

    //campaign upload directory and path
    {
//        $wl_upload_folder_name = 'web-lister';
//        $wl_upload_dir = trailingslashit(wp_upload_dir()['basedir']) . "$wl_upload_folder_name/";
//        if (!is_dir($wl_upload_dir))
//            @mkdir($wl_upload_dir);
//        if (!is_dir($wl_upload_dir))
//            user_error("Unable to create directory <b>" . $wl_upload_dir . "</b><hr />Please make the folder writable", E_ERROR);
//        $wl_upload_dir_url = trailingslashit(wp_upload_dir()['baseurl']) . "$wl_upload_folder_name/";
//        define('WL_UPLOAD_DIR', $wl_upload_dir);
//        define('WL_UPLOAD_DIR_URL', $wl_upload_dir_url);
//
//        //reset used variables
//        unset($wl_upload_folder_name);
//        unset($wl_upload_dir);
//        unset($wl_upload_dir_url);
    }
}