(function ($) {
    $(document).ready(function () {
	$('.mlw_next, .mlw_previous').click(function () {
	    var valuedata = [];
	    var radiolength = jQuery('.qmn_radio_answers').length;
	    for (var i = 1; i <= radiolength; i++) {
		var new_value = jQuery('input[name= question' + i + ']:checked').val();
		var push_data = new_value + "_" + i;
		valuedata.push(push_data);
	    }
	    $.post(ajax_url, {
		action: 'qfc_get_question_answer',
		question_wise_answer_data: valuedata,
	    }, function (res) {
		mydatajson = $.parseJSON(res);
		asd(mydatajson);
		function asd(mydatajson) {
		    if(mydatajson.length !== 0){
                    $('#qfc_container_pie').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: null,
			    plotShadow: false,
			    type: 'pie'
			},
			title: {
			    text: 'Session Statistics'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: true,
				    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				    style: {
					color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
				    }
				}
			    }
			},
			series: [{
			    colorByPoint: true,
			    data: mydatajson
			}]
		    });
                    }
		}
                jQuery('.highcharts-legend').css('display','none');
                if(jQuery('.mlw_qmn_comment_section_text').parent().css('display') == 'block'){
  jQuery('#qfc_container_pie').css({'display':'none'});
}
	    });
	});
    });
})(jQuery);
