<?php
session_start();
if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}

if (!function_exists('vd')) {

    function vd($e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }

}

function qfc_add_filters_to_quiz($display, $qmn_quiz_options) {
    if (isset($_POST['complete_quiz']))
        return $display;
    global $wpdb;
    //get categories for quiz
    {
        $quiz_id = $qmn_quiz_options->quiz_id;
        $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT question_type FROM " . $wpdb->prefix . "mlw_questions WHERE deleted='0' AND quiz_id = %d", $quiz_id));
        $quiz_type = Array();
        foreach ($mlw_quiz_data as $mlw_quiz) {
            $quiz_type[] = $mlw_quiz->question_type;
        }
        if (isset($_POST['qfc_filter'])) {
            $_SESSION['qfc_filters'] = '';
            $_SESSION['qfc_filters'][$quiz_id]['category'] = $_POST['qfc_filter']['category'];
            $_SESSION['qfc_filters'][$quiz_id]['question_type'] = $_POST['qfc_filter']['question_type'];
            $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] = $_POST['qfc_filter']['question_difficulty_level'];
            $quiz_post_id = qfc_get_quiz_post_id($quiz_id);
            $redirect = get_permalink($quiz_post_id);
            echo "<h2>Applying Filters</h2><hr />";
            echo "<script>window.location = '$redirect';</script>";
            die;
        }
        $quiz_categories = $wpdb->get_results($wpdb->prepare("SELECT category FROM " . $wpdb->prefix . "mlw_questions WHERE quiz_id=%d AND deleted='0' GROUP BY category", $quiz_id));
    }
    ob_start();
    //Load Question Types
    global $mlwQuizMasterNext;
    $qmn_question_types = $mlwQuizMasterNext->pluginHelper->get_question_type_options();
    // print_r( $qmn_question_types);die;
    ?>
    <div>
        <style>select {width:22%}</style>
        <form method="post">
            <h4>Filters</h4>
            <select name="qfc_filter[category]">
                <option value="">Select Category</option>
                <?php foreach ($quiz_categories as $quiz_category) { ?>
                    <?php if ($quiz_category->category === $_SESSION['qfc_filters'][$quiz_id]['category']) { ?>
                        <option selected><?php echo $quiz_category->category ?></option>
                    <?php } else { ?>
                        <option><?php echo $quiz_category->category ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <select name="qfc_filter[question_type]">
                <option value="">Select Question Type</option>
                <?php foreach ($qmn_question_types as $qmn_question_typ) { ?>
                    <?php if (in_array($qmn_question_typ['slug'], $quiz_type) && $qmn_question_typ['slug'] === $_SESSION['qfc_filters'][$quiz_id]['question_type']) { ?>
                        <option  value ="<?php echo $qmn_question_typ['slug']; ?>" selected><?php echo $qmn_question_typ['name'] ?></option>
                    <?php } else if (in_array($qmn_question_typ['slug'], $quiz_type) && $qmn_question_typ['slug'] !== $_SESSION['qfc_filters'][$quiz_id]['question_type']) { ?>
                        <option value ="<?php echo $qmn_question_typ['slug']; ?>"><?php echo $qmn_question_typ['name'] ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <select name="qfc_filter[question_difficulty_level]">
                <option value="">Select difficulty Level</option>
                <?php
                $question_difficulty_levels = get_option(QFC_DIFFICULTIES);
                foreach ($question_difficulty_levels as $key => $value) {
                    ?>
                    <?php if ($key === $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level']) { ?>
                        <option  value ="<?php echo $key; ?>" selected><?php echo $value ?></option>
                    <?php } else { ?>
                        <option value ="<?php echo $key; ?>"><?php echo $value ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <button type="submit">Apply Filters</button>
        </form>
    </div>
    <hr />
    <?php
    return $display . ob_get_clean();
}

function qfc_get_quiz_post_id($quiz_id) {
    $quiz_post_id = get_posts(array(
        'post_type' => 'quiz',
        'posts_per_page' => 1,
        'fields' => 'ids',
        'meta_key' => 'quiz_id',
        'meta_value' => $quiz_id,
    ));
    wp_reset_query();
    if (empty($quiz_post_id)) {
        return null;
    }
    return array_shift($quiz_post_id);
}

function qfc_apply_filters_to_quiz($query, $quiz_id) {
    if ($_SESSION['qfc_filters'][$quiz_id]['category'] === '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] === '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] === '') {
        
    } else if ($_SESSION['qfc_filters'][$quiz_id]['category'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] === '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] === '') {

        $query = $query . " AND category='" . $_SESSION['qfc_filters'][$quiz_id]['category'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['question_type'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['category'] === '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] === '') {

        $query = $query . " AND question_type='" . $_SESSION['qfc_filters'][$quiz_id]['question_type'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] === '' && $_SESSION['qfc_filters'][$quiz_id]['category'] === '') {

        $query = "SELECT * FROM wp_mlw_questions INNER JOIN wp_mlw_question_difficulty ON wp_mlw_questions.quiz_id=wp_mlw_question_difficulty.quiz_id AND wp_mlw_questions.question_id=wp_mlw_question_difficulty.question_id WHERE wp_mlw_questions.quiz_id=%d AND wp_mlw_questions.deleted=0 and wp_mlw_question_difficulty.difficulty_level = '" . $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['category'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] === '') {

        $query = $query . " AND category='" . $_SESSION['qfc_filters'][$quiz_id]['category'] . "' AND question_type='" . $_SESSION['qfc_filters'][$quiz_id]['question_type'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['category'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] === '') {

        $query = "SELECT * FROM wp_mlw_questions INNER JOIN wp_mlw_question_difficulty ON wp_mlw_questions.quiz_id=wp_mlw_question_difficulty.quiz_id AND wp_mlw_questions.question_id=wp_mlw_question_difficulty.question_id WHERE wp_mlw_questions.category='" . $_SESSION['qfc_filters'][$quiz_id]['category'] . "' AND wp_mlw_questions.quiz_id=%d AND wp_mlw_questions.deleted=0 and wp_mlw_question_difficulty.difficulty_level = '" . $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['question_type'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['category'] === '') {

        $query = "SELECT * FROM wp_mlw_questions INNER JOIN wp_mlw_question_difficulty ON wp_mlw_questions.quiz_id=wp_mlw_question_difficulty.quiz_id AND wp_mlw_questions.question_id=wp_mlw_question_difficulty.question_id WHERE wp_mlw_questions.question_type='" . $_SESSION['qfc_filters'][$quiz_id]['question_type'] . "' AND wp_mlw_questions.quiz_id=%d AND wp_mlw_questions.deleted=0 and wp_mlw_question_difficulty.difficulty_level = '" . $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] . "'";
    } else if ($_SESSION['qfc_filters'][$quiz_id]['category'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_type'] !== '' && $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] !== '') {

        $query = "SELECT * FROM wp_mlw_questions INNER JOIN wp_mlw_question_difficulty ON wp_mlw_questions.quiz_id=wp_mlw_question_difficulty.quiz_id AND wp_mlw_questions.question_id=wp_mlw_question_difficulty.question_id WHERE wp_mlw_questions.category='" . $_SESSION['qfc_filters'][$quiz_id]['category'] . "' AND wp_mlw_questions.question_type='" . $_SESSION['qfc_filters'][$quiz_id]['question_type'] . "' AND wp_mlw_questions.quiz_id=%d AND wp_mlw_questions.deleted=0 and wp_mlw_question_difficulty.difficulty_level = '" . $_SESSION['qfc_filters'][$quiz_id]['question_difficulty_level'] . "'";
    }
    return $query;
}

/* function qfc_mlw_qmn_ud_after_quiz_options() {
  $graph_data = qfc_mlw_qmn_json_data_for_chart();
  echo $graph_data;
  die;
  ?>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <div id="qfc_container_line" style="height: 400px"></div>
  <div id="qfc_container_bar" style="height: 400px;margin-top:10px;"></div>
  <script type = "text/javascript">
  $j = jQuery.noConflict();
  var options = {
  chart: {
  renderTo: 'qfc_container_line',
  type: 'line',
  marginRight: 130,
  marginBottom: 25
  },
  title: {
  text: 'Quizzes Vs Average Score',
  x: -20 //center
  },
  subtitle: {
  text: '',
  x: -20
  },
  xAxis: {
  categories: []
  },
  yAxis: {
  title: {
  text: 'Average(%)'
  },
  plotLines: [{
  value: 0,
  width: 1,
  color: '#808080'
  }]
  },
  tooltip: {
  formatter: function () {
  return '<b>' + this.series.name + '</b><br/>' +
  this.x + ': ' + this.y;
  }
  },
  legend: {
  layout: 'vertical',
  align: 'right',
  verticalAlign: 'top',
  x: -10,
  y: 100,
  borderWidth: 0
  },
  series: []
  };
  var graphdata = $j.parseJSON('<?php echo $graph_data ?>');
  var graphcat = $j.map(graphdata.categories, function (el) {
  return el;
  });
  options.xAxis.categories = graphcat;
  options.series = $j.makeArray(graphdata.result);
  new Highcharts.Chart(options);

  var options1 = {
  chart: {
  renderTo: 'qfc_container_bar',
  type: 'bar',
  },
  title: {
  text: 'Quizzes Vs Average Score'
  },
  xAxis: {
  categories: [],
  title: {
  text: null
  }
  },
  yAxis: {
  min: 0,
  title: {
  text: 'Average(%)',
  align: 'high'
  },
  labels: {
  overflow: 'justify'
  }
  },
  tooltip: {
  valueSuffix: 'Average(%)'
  },
  plotOptions: {
  bar: {
  dataLabels: {
  enabled: true
  }
  }
  },
  legend: {
  layout: 'vertical',
  align: 'right',
  verticalAlign: 'top',
  x: -40,
  y: 80,
  floating: true,
  borderWidth: 1,
  backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
  shadow: true
  },
  credits: {
  enabled: false
  },
  series: []
  };
  var graphdata = $j.parseJSON('<?php echo $graph_data ?>');
  var graphcat = $j.map(graphdata.categories, function (el) {
  return el;
  });
  options1.xAxis.categories = graphcat;
  options1.series = $j.makeArray(graphdata.result);
  var chart = new Highcharts.Chart(options1);
  </script>
  <?php
  } */

function qfc_mlw_qmn_json_data_for_chart() {
    global $wpdb;
    $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0'  ORDER BY quiz_id DESC"));
    if (!isset($mlw_quiz_data) || empty($mlw_quiz_data)) {
        return null;
    }
    $cat_name = $wpdb->get_results($wpdb->prepare("SELECT quiz_name FROM wp_mlw_quizzes WHERE deleted = '0' ORDER BY quiz_id DESC"));
    $cat_data_array = array();
    $k = 1;
    foreach ($cat_name as $cat) {
        $cat_data_array[$k] = $cat->quiz_name;
        $k++;
    }
    $result1 = array_values(qfc_mlw_get_chart_formatted_array($mlw_quiz_data, $cat_data_array));

    $current_user = wp_get_current_user();
    $currnt_user_name = $current_user->display_name;
    foreach ($result1 as $key => $val) {
        if ($val['name'] === $currnt_user_name) {
            $result1[$key]['lineWidth'] = 6;
        }
    }
    //echo "<pre/>";print_r($result1);die;
    $graph_data1 = array('categories' => $cat_data_array, 'result' => $result1);

    $result = json_encode($graph_data1);
    die;
}

function qfc_mlw_get_chart_formatted_array($mlw_quiz_data, $cat_data_array) {
    $result = array();
    foreach ($mlw_quiz_data as $mlw_quiz_dat) {
        if (count($result) == 0) {
            $result[$mlw_quiz_dat->name][str_replace(" ", "_", $mlw_quiz_dat->quiz_name)]['correct_score'][] = $mlw_quiz_dat->correct_score;
        } else {
            foreach ($result as $key => $val) {
                if ($key === $mlw_quiz_dat->name) {
                    foreach ($val as $key1 => $va) {
                        if (str_replace("_", " ", $key1) === $mlw_quiz_dat->quiz_name) {
                            //echo "here";die;
                            $result[$mlw_quiz_dat->name][str_replace(" ", "_", $mlw_quiz_dat->quiz_name)]['correct_score'][] = $mlw_quiz_dat->correct_score;
                        } else {
                            $result[$mlw_quiz_dat->name][str_replace(" ", "_", $mlw_quiz_dat->quiz_name)]['correct_score'][] = $mlw_quiz_dat->correct_score;
                        }
                    }
                } else {
                    $result[$mlw_quiz_dat->name][str_replace(" ", "_", $mlw_quiz_dat->quiz_name)]['correct_score'][] = $mlw_quiz_dat->correct_score;
                }$j++;
            }
        }
    }
    //echo "<pre/>";print_r($result);die;
    $new_result = qfc_mlw_get_chart_final_array($result);
    return $new_result;
}

function qfc_mlw_get_chart_final_array($result) {
    $new_result = array();
    $k = 1;
    foreach ($result as $key => $val) {
        $new_result[$k]['name'] = $key;
        $score_length = count($val);
        foreach ($val as $va) {
            if ($score_length > 0) {
                $new_result[$k]['data'][] = round(array_sum($va['correct_score']) / count($va['correct_score']), 2);
            }
            $score_length--;
        }
        $k++;
    }
    return $new_result;
}

function qfc_show_question_difficulty() {
    global $wpdb;
    $default_difficulties = array(
        'easy' => 'Easy',
        'medium' => 'Medium',
        'hard' => 'Hard',
    );
    $question_difficulty_levels = get_option(QFC_DIFFICULTIES);
    $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0'  ORDER BY quiz_id DESC"));
    ?>
    <div class="row">
        <label class="option_label"><?php _e('Question Difficulty'); ?></label>
        <select name = "qfc_question_difficulty_level" id = "qfc_question_difficulty_level">
            <?php foreach ($question_difficulty_levels as $key => $value) { ?>
                <option value ="<?php echo $key; ?>"><?php echo $value; ?></optin>
                <?php } ?>
        </select>
    </div>
    <script>
        (function ($) {
            $("#the-list").on('click', '.edit_link', function () {
                qfc_check_for_question_id();
            });
            function qfc_check_for_question_id() {
                if ($('#question_id').val() != '0') {
                    var ajax_url = '<?php echo admin_url('admin-ajax.php') ?>';
                    var question_id = $('#question_id').val();
                    var quiz_id = $("input[name=quiz_id]").val();
                    $.post(ajax_url, {
                        action: 'qfc_get_question_difficulty',
                        quiz_id: quiz_id,
                        question_id: question_id,
                    }, function (response) {
                        var resultVal = response;
                        $('#qfc_question_difficulty_level option').each(function () {
                            if ($(this).attr('value') == resultVal) {
                                $(this).attr('selected', true);
                            }
                        });
                    });
                } else {
                    setTimeout(qfc_check_for_question_id, 100);
                }
            }
        })(jQuery);</script>
    <?php
}

function qfc_get_question_difficulty() {
    global $wpdb;
    $quiz_id = $_POST['quiz_id'];
    $question_id = $_POST['question_id'];
    $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT difficulty_level FROM " . $wpdb->prefix . "mlw_question_difficulty WHERE quiz_id=%d AND question_id = %d", $quiz_id, $question_id));
    echo $quiz_difficulty_level = $mlw_quiz_data[0]->difficulty_level;
    die;
}

function qfc_insert_question_difficulty($question_submission, $quiz_id, $question_id, $question_difficulty_level) {
    global $wpdb;
    if ($question_submission == 'new_question') {
        $results = $wpdb->insert(
                $wpdb->prefix . "mlw_question_difficulty", array(
            'quiz_id' => $quiz_id,
            'question_id' => $question_id,
            'difficulty_level' => $question_difficulty_level,
                ), array(
            '%d',
            '%d',
            '%s',
                )
        );
    }if ($question_submission == 'edit_question') {
        $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT id FROM " . $wpdb->prefix . "mlw_question_difficulty WHERE quiz_id=%d AND question_id = %d", $quiz_id, $question_id));
        $row_id = $mlw_quiz_data[0]->id;

        if ($row_id) {
            $results = $wpdb->update(
                    $wpdb->prefix . "mlw_question_difficulty", array(
                'quiz_id' => $quiz_id,
                'difficulty_level' => $question_difficulty_level,
                    ), array('id' => $row_id), array(
                '%d',
                '%s',
                    ), array('%d')
            );
        } else {
            //echo "here2";die;
            $results = $wpdb->insert(
                    $wpdb->prefix . "mlw_question_difficulty", array(
                'quiz_id' => $quiz_id,
                'question_id' => $question_id,
                'difficulty_level' => $question_difficulty_level,
                    ), array(
                '%d',
                '%d',
                '%s',
                    )
            );
        }
    }
}

function qfc_mlw_qmn_ud_after_question($ret, $mlw_quiz_results_array) {
    global $wpdb;
    $user = wp_get_current_user();
    $user_name = $user->display_name;

    $graph_data = array();
    foreach ($mlw_quiz_results_array as $mlw_quiz_results) {
        if ($mlw_quiz_results['correct'] === 'correct') {
            $graph_data[$mlw_quiz_results['correct']]['name'] = 'correct';
            $graph_data[$mlw_quiz_results['correct']]['y'][] = (int) 100;
        } else {
            $graph_data['incorrect']['name'] = 'incorrect';
            $graph_data['incorrect']['y'][] = (int) 0;
        }
    }
    $new_graph_data = array();
    if (count($graph_data) == 1) {
        if ($graph_data['name'] === 'correct') {
            $new_graph_data[1]['name'] = 'incorrect';
            $graph_data['data'][1]['y'] = (int) (100 - $graph_data['data'][0]['name']);
        } else {
            
        }
    }
    ob_start();
    ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <div id="qfc_container_pie" style="min-width: 310px; height: 400px; max-width: 600px;"></div>
    <script type = "text/javascript">
        $j = jQuery.noConflict();
        $j(function () {
            var s_data = [];
    <?php foreach ($graph_data as $g_data) { ?>
                var s_data_val = {
                    name: '<?php echo $g_data['name'] ?>',
                    y: <?php echo $g_data['y'] ?>,
                };
                s_data.push(s_data_val);
    <?php } ?>
            var opts = {
                chart: {
                    renderTo: 'qfc_container_pie',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Average Score For Cuerrent Session'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                        name: '<?php echo $user_name; ?>',
                        data: s_data
                    }]
            };
            new Highcharts.Chart(opts);
        });
    </script>
    <?php
    return $ret . ob_get_clean();
}

// add graphs on user dashboard
function qfc_add_graph_on_dashboard($ret) {
    global $wpdb;
    $user = wp_get_current_user();
    $user_name = $user->display_name;
    $mlw_quiz_id = $wpdb->get_results($wpdb->prepare("SELECT quiz_id,quiz_name FROM " . $wpdb->prefix . "mlw_quizzes WHERE deleted=0 LIMIT 1"));    //get quiz id
    $category_subject_data = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT(category) FROM " . $wpdb->prefix . "mlw_questions WHERE deleted=0"));     //get category names from db
    $cat_subject_data = array(); // convert db data to php array
    foreach ($category_subject_data as $category_dat) {
        $cat_subject_data[] = $category_dat->category;
    }
    $category_result_data = $wpdb->get_results($wpdb->prepare("SELECT cat_name,AVG(correct_score) as avg FROM wp_mlw_modified_results WHERE deleted=0 AND user_id =%d GROUP BY cat_name", $user->id)); // get result of logged-in user category wise
    $category_result = array();
    foreach ($category_result_data as $category_result_dat) {
        $category_result[$category_result_dat->cat_name] = $category_result_dat->avg;
    }
    foreach ($cat_subject_data as $cat_subject_dat) {
        if (!in_array($cat_subject_dat, array_keys($category_result))) {
            $category_result[$cat_subject_dat] = 0;
        }
    }
    $final_cat_result = array('0' => array('name' => $user_name));
    foreach ($category_result as $val) {
        $final_cat_result[0]['data'][] = (float) $val;
    }
    $quiz_id = $mlw_quiz_id[0]->quiz_id;
    $mlw_quiz_data = $wpdb->get_results($wpdb->prepare("SELECT name, AVG(correct_score) AS avg_score FROM " . $wpdb->prefix . "mlw_results WHERE deleted=0 AND quiz_id =%d GROUP BY name", $quiz_id));
    $result = array();
    foreach ($mlw_quiz_data as $mlw_quiz) {
        if ($mlw_quiz->name === $user_name) {
            $result[1]['name'] = $mlw_quiz->name;
            $result[1]['data'][] = round($mlw_quiz->avg_score, 2);
        } else {
            $result[2]['name'] = 'others';
            $result[2]['data'][] = round($mlw_quiz->avg_score, 2);
        }
    }

    foreach ($result as $key => $value) {
        if ($value['name'] !== $user_name) {
            $avg_data = round((array_sum($value['data']) / count($value['data'])), 2);
            unset($result[$key]['data']);
            $result[$key]['data'][] = $avg_data;
        }
    }
    if (!isset($result) || empty($result)) {
        return $ret;
    }
    if (count($result) == 1) {
        if ($result[1]['name'] === $user_name) {
            array_push($result, array(
                'name' => 'others',
                'data' => array(
                    '0' => 0
                ))
            );
        }
    }
    $graph_line_data_array = array('categories' => $mlw_quiz_id[0]->quiz_name, 'result' => array_values($result));

    $graph_bar_data_array = array('categories' => $cat_subject_data, 'result' => array_values($final_cat_result));
    $json_graph_line_data = json_encode($graph_line_data_array);
    $json_graph_bar_data = json_encode($graph_bar_data_array);
    ob_start();
    ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <div id="qfc_container_line" style="height: 300px"></div>
    <div id="qfc_container_bar" style="height: 400px;margin-top:50px;"></div>
    <script type = "text/javascript">
        $j = jQuery.noConflict();
        var options = {
            chart: {
                renderTo: 'qfc_container_line',
                type: 'bar',
            },
            title: {
                text: 'Performance w.r.t. other users'
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Average(%)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: 'Average(%)'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: []
        };
        var graphdata = $j.parseJSON('<?php echo $json_graph_line_data ?>');
        options.xAxis.categories = ["" + graphdata.categories + ""];
        options.series = $j.makeArray(graphdata.result);
        new Highcharts.Chart(options);

        var options1 = {
            chart: {
                renderTo: 'qfc_container_bar',
                type: 'bar',
            },
            title: {
                text: 'Performance w.r.t Subjects'
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Average(%)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: 'Average(%)'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: []
        };
        var graphdata = $j.parseJSON('<?php echo $json_graph_bar_data ?>');
        var graphcat = $j.map(graphdata.categories, function (el) {
            return el;
        });
        options1.xAxis.categories = graphcat;
        console.log(options1.xAxis.categories);
        options1.series = $j.makeArray(graphdata.result);
        var chart = new Highcharts.Chart(options1);
    </script>
    <?php
    return $ret . ob_get_clean();
}

function qfc_after_saving_user_result($new_table_result_array, $mlw_quiz_results_array) {
    global $wpdb;
    $new_result_array = array();
    foreach ($mlw_quiz_results_array as $mlw_quiz_results) {
        if (count($new_result_array) == 0) {
            $new_result_array[$mlw_quiz_results['category']]['name'] = $mlw_quiz_results['category'];
            if ($mlw_quiz_results['correct'] === 'correct') {
                $new_result_array[$mlw_quiz_results['category']]['score'][] = 100;
            } else {
                $new_result_array[$mlw_quiz_results['category']]['score'][] = 0;
            }
        } else {
            if (in_array(array_keys($new_result_array), $mlw_quiz_results['category'])) {
                if ($mlw_quiz_results['correct'] === 'correct') {
                    $new_result_array[$mlw_quiz_results['category']]['score'][] = 100;
                } else {
                    $new_result_array[$mlw_quiz_results['category']]['score'][] = 0;
                }
            } else {
                $new_result_array[$mlw_quiz_results['category']]['name'] = $mlw_quiz_results['category'];
                if ($mlw_quiz_results['correct'] === 'correct') {
                    $new_result_array[$mlw_quiz_results['category']]['score'][] = 100;
                } else {
                    $new_result_array[$mlw_quiz_results['category']]['score'][] = 0;
                }
            }
        }
    }
    //pr($new_result_array);die;
    $values_data = '';
    foreach ($new_result_array as $new_result) {
        $values_data .= "('" . $new_table_result_array['quiz_id'] . '\',\'' . $new_table_result_array['user'] . '\',\'' . $new_result['name'] . '\',\'' . $new_table_result_array['quiz_name'] . '\',\'' . (round((array_sum($new_result['score']) / count($new_result['score'])), 2)) . '\',\'' . $new_table_result_array['name'] . '\',\'' . '0\'),';
    }
    $values_data = rtrim($values_data, ',');
    $query = "INSERT INTO " . $wpdb->prefix . "mlw_modified_results (quiz_id,user_id,cat_name,quiz_name,correct_score,user_name,deleted) VALUES " . $values_data;
    $wpdb->query($query);
    $values_data1 = '';
    $new_result_array1 = array();
    $j = 0;
    foreach ($mlw_quiz_results_array as $mlw_quiz_results) {
        $new_result_array1[$j]['quiz_id'] = $new_table_result_array['quiz_id'];
        $new_result_array1[$j]['user_id'] = $new_table_result_array['user'];
        $new_result_array1[$j]['cat_name'] = $mlw_quiz_results['category'];
        $new_result_array1[$j]['question_name'] = $mlw_quiz_results[0];
        $new_result_array1[$j]['selected_option'] = $mlw_quiz_results[1];
        $new_result_array1[$j]['quiz_name'] = $new_table_result_array['quiz_name'];
        $new_result_array1[$j]['correct_score'] = ($mlw_quiz_results['correct'] === 'correct') ? 100 : 0;
        $new_result_array1[$j]['user_name'] = $new_table_result_array['name'];
        $j++;
    }
    foreach ($new_result_array1 as $new_result) {
        $values_data1 .= "('" . $new_result['quiz_id'] . '\',\'' . $new_result['user_id'] . '\',\'' . $new_result['cat_name'] . '\',\'' . $new_result['question_name'] .'\',\'' . $new_result['selected_option'] . '\',\'' . $new_result['quiz_name'] . '\',\'' . $new_result['correct_score'] . '\',\'' . $new_result['user_name'] . '\',\'' . '0\'),';
    }
    $values_data1 = rtrim($values_data1, ',');
    $query1 = "INSERT INTO " . $wpdb->prefix . "mlw_modified_results1 (quiz_id,user_id,cat_name,question_name,selected_option,quiz_name,correct_score,user_name,deleted) VALUES " . $values_data1;
    $wpdb->query($query1);
}

function qfc_add_graph_on_each_question($ret, $quiz_id, $category_name, $question_name, $div_val) {
    global $wpdb;
    $user = wp_get_current_user();
    $user_name = $user->display_name;
    $mlw_question_data = $wpdb->get_results("SELECT answer_array FROM " . $wpdb->prefix . "mlw_questions WHERE deleted=0 AND quiz_id ='" . $quiz_id . "' AND question_name ='" . $question_name . "'");
    $new_question_data = unserialize($mlw_question_data[0]->answer_array);
    $question_array = array();
    foreach($new_question_data as $new_question){
        $question_array[] = $new_question[0];
    }
    $mlw_quiz_data1 = $wpdb->get_results("SELECT COUNT(correct_score) as user_count,correct_score FROM " . $wpdb->prefix . "mlw_modified_results1 WHERE deleted=0 AND quiz_id ='" . $quiz_id . "' AND cat_name ='" . $category_name . "' AND question_name ='" . $question_name . "' AND user_name <>'" . $user_name . "'  GROUP BY correct_score");
    $mlw_quiz_data11 = $wpdb->get_results("SELECT COUNT(selected_option) as user_count,selected_option FROM " . $wpdb->prefix . "mlw_modified_results1 WHERE deleted=0 AND quiz_id ='" . $quiz_id . "' AND cat_name ='" . $category_name . "' AND question_name ='" . $question_name . "' AND user_name <>'" . $user_name . "'  GROUP BY selected_option");
    $jj  = 0;
    $mlw_quiz_data_data = array();
    foreach($mlw_quiz_data11 as $mlw_quiz_d){
        $mlw_quiz_data_data['name'][] = $mlw_quiz_d->selected_option;
        $mlw_quiz_data_data[$jj]['name'] = $mlw_quiz_d->selected_option;
        $mlw_quiz_data_data[$jj]['y'] = (int)$mlw_quiz_d->user_count;
        $jj++;
    }
    $new_result_for_bar_chart_count1 = array();
    $jq1 = 0;
    if (count($mlw_quiz_data_data)== 0) {
        foreach ($question_array as $question_arr) {
            $new_result_for_bar_chart_count1['data'][$jq1]['name'] = $question_arr;
            $new_result_for_bar_chart_count1['data'][$jq1]['y'] = (int) 0;
            $jq1++;
        }
    } else {
        foreach ($question_array as $question_arr) {
            if(!in_array($question_arr,array_values($mlw_quiz_data_data['name']))){
                array_push($mlw_quiz_data_data,array('name'=>$question_arr,'y'=>(int)0));
            }
        }
        unset($mlw_quiz_data_data['name']);
        $new_result_for_bar_chart_count1['data'] = $mlw_quiz_data_data;
    }
    $category_data = array();
    foreach($new_result_for_bar_chart_count1['data'] as $new_result_for_ba){
        $category_data[] = $new_result_for_ba['name'];
    }
    asort($question_array);
    $tmp12 = $new_result_for_bar_chart_count1['data'];
    usort($tmp12, 'compareByName');
    unset($new_result_for_bar_chart_count1['data']);
    $new_result_for_bar_chart_count1['data']  = $tmp12;
    //pr($question_array);pr($new_result_for_bar_chart_count1);die;
    $graph_data3 = json_encode($new_result_for_bar_chart_count1);
    $graph_data33 = json_encode($question_array);
    ob_start();
    ?>
    <?php
    global $qfc_scripts_included;
    if (!$qfc_scripts_included) {
        ?>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <?php $qfc_scripts_included = true; ?>
    <?php } ?>


    <div id="qfc_container_bar1_<?php echo $div_val; ?>" style="height: 300px;margin-bottom: 74px;margin-top: 113px;max-width: 300px;min-width: 300px;position: absolute;right: 10px;"></div>
    <script type = "text/javascript">
        $j = jQuery.noConflict();
        var options1 = {
            chart: {
                renderTo: 'qfc_container_bar1_<?php echo $div_val; ?>',
                type: 'bar',
            },
            title: {
                text: 'Answer Statistics'
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: 'count'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: []
        };
        var graphdata1 = $j.parseJSON('<?php echo $graph_data33 ?>');
        var graphdata = $j.parseJSON('<?php echo $graph_data3 ?>');
        options1.categories = graphdata1;
        options1.series = $j.makeArray(graphdata);
        var chart = new Highcharts.Chart(options1);
        $j('.highcharts-legend').css('display','none');
    </script>
    <?php
    return $ret . ob_get_clean();
}

function qfc_get_question_answer() {
    global $wpdb;
    $user = wp_get_current_user();
    $user_name = $user->display_name;
    $ajax_data = $_POST;
    $result_array = array();
    $final_array = array();
    $final_array_for_pie_chart = array();
    foreach ($ajax_data['question_wise_answer_data'] as $data) {
        $pos = strpos($data, 'No Answer');
        if ($pos === false) {
            $get_id_data = explode('_', $data);
            if (is_array($get_id_data)) {
                $result_array[$get_id_data[1]] = $get_id_data[0];
            }
        }
    }
    if (count($result_array) > 0) {
        $quiz_id = implode(',', array_keys($result_array));
        $mlw_quiz_data1 = $wpdb->get_results("SELECT question_id,answer_array FROM " . $wpdb->prefix . "mlw_questions WHERE deleted=0 AND question_id IN (" . $quiz_id . ")");
        foreach ($mlw_quiz_data1 as $mlw_quiz_dat) {
            $correct_result = '';
            $answer_data = unserialize($mlw_quiz_dat->answer_array);
            foreach ($answer_data as $answer_d) {
                if ($answer_d[2] == 1) {
                    $correct_result = $answer_d[0];
                    break;
                }
            }
            $final_array[$mlw_quiz_dat->question_id] = $correct_result;
        }
        //pr($result_array);pr($final_array);
        $resultant = array_diff($result_array, $final_array);
        if (count($resultant) == 0) {
            $final_array_for_pie_chart[0]["name"] = "correct";
            $final_array_for_pie_chart[0]["y"] = 100;
            $final_array_for_pie_chart[1]["name"] = "in-correct";
            $final_array_for_pie_chart[1]["y"] = 0;
        } else {
            $correct_percentage = round((((count($result_array) - count($resultant)) / count($result_array)) * 100), 2);
            $final_array_for_pie_chart[0]["name"] = "correct";
            $final_array_for_pie_chart[0]["y"] = $correct_percentage;
            $final_array_for_pie_chart[1]["name"] = "in-correct";
            $final_array_for_pie_chart[1]["y"] = (100 - $correct_percentage);
        }
    }
    $new_json_data = json_encode($final_array_for_pie_chart);
    echo $new_json_data;
    die;
}

function get_question_answer_to_make_graph() {
    $handle = 'qfc_question_js';
    wp_register_script($handle, QFC_DIR_URL . 'js/abc.js', array('jquery'), null, true);
    wp_localize_script($handle, 'ajax_url', admin_url('admin-ajax.php'));
    wp_enqueue_script($handle);
}
function compareByName($tmp12, $b) {
    return strcmp($tmp12["name"], $b["name"]);
}